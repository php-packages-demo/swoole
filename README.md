# swoole

[swoole](https://pecl.php.net/package/swoole) Event-driven asynchronous and concurrent networking engine with high performance for PHP

* https://repology.org/project/php:swoole
* ~https://phppackages.org/s/swoole~

# Unofficial documentation
* [*Serving High Performance the Symfony App with Swoole/Docker*
  ](https://medium.com/beyn-technology/serving-high-performance-the-symfony-app-with-swoole-docker-758d8f176889)
  2022-09 Emre Çalışkan
* [*Let’s Tackle PHP Swoole Solemnly*
  ](https://medium.com/beyn-technology/lets-tackle-php-swoole-solemnly-18e424731fa8)
  2022-03 Mert Simsek
